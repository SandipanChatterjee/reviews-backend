const mongoose = require("mongoose");
const schema = mongoose.Schema;
const watchListSchema = new schema({
  watchList: {
    type: Boolean,
    required: [true, "Please enter watchlist"],
    default: false,
  },
  movie: {
    type: mongoose.Schema.ObjectId,
    ref: "Movies",
    required: true,
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: true,
  },
});

module.exports = mongoose.model("WatchList", watchListSchema);
