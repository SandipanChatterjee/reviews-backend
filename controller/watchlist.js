const WatchList = require("../model/watchlist");
const User = require("../model/user");
const asyncHanlder = require("../middleware/asynchandler");
const ErrorHandler = require("../middleware/errorhandler");

// @desc      GET WatchList
// @route     GET /api/v1/user/:userId/watchlist
// @access    Private
exports.getWatchList = asyncHanlder(async (req, res, next) => {
  const user = await User.findById(req.params.userId);
  console.log(user);
  if (!user) {
    return ErrorHandler("User not found", res, next);
  }
  const query = await WatchList.find({ user: req.params.userId });
  if (!user) {
    return ErrorHandler("Watch list cannot be created", res, next);
  }
  res.status(200).json({
    success: true,
    data: query,
  });
});

// @desc      CREATE WatchList
// @route     POST /api/v1/user/:userId/watchlist
// @access    Private
exports.createWatchList = asyncHanlder(async (req, res, next) => {
  const user = await User.findById(req.params.userId);
  console.log(user);
  if (!user) {
    return ErrorHandler("User not found", res, next);
  }
  req.body.user = req.params.userId;
  const query = await WatchList.create(req.body);
  if (!user) {
    return ErrorHandler("Watch list cannot be created", res, next);
  }
  res.status(200).json({
    success: true,
    data: query,
  });
});

// @desc      UPDATE WatchList
// @route     PUT /api/v1/watchlist/:id
// @access    Private
exports.updateWatchList = asyncHanlder(async (req, res, next) => {
  const watchlist = await WatchList.findById(req.params.id);
  if (!watchlist) {
    return ErrorHandler("WatchList not found", res, next);
  }
  const query = await WatchList.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });
  if (!query) {
    return ErrorHandler("Watch list cannot be created", res, next);
  }
  res.status(200).json({
    success: true,
    data: query,
  });
});
