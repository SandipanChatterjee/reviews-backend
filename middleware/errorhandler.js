const errorHandler = (errMsg, res, next) => {
  console.log("ErrorHandler called");
  res.status(404).json({ error: errMsg });
  next();
};

module.exports = errorHandler;
