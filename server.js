const express = require("express");
const dotenv = require("dotenv");
const errorHandler = require("./middleware/errorhandler");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const mongoSanitize = require("express-mongo-sanitize");
const helmet = require("helmet");
const xss = require("xss-clean");
const rateLimit = require("express-rate-limit");

//mongodb
const connectDB = require("./config/db");
connectDB();

//Route files
const reviews = require("./routes/reviews");
const movies = require("./routes/movies");
const trailer = require("./routes/trailer");
const auth = require("./routes/auth");
const location = require("./routes/location");
const rating = require("./routes/rating");
const user = require("./routes/user");
const watchlist = require("./routes/watchlist");

// Load env vars
dotenv.config({ path: "./config/config.env" });

const app = express();

//cookie parse
app.use(cookieParser());

//Body parse
app.use(express.json());

//CORS
app.use(cors());

//make uploads folder public
app.use("/uploads", express.static("uploads"));

//sanitize data
app.use(mongoSanitize());

//set security header
app.use(helmet());

//Prevent xss attacks
app.use(xss());

//limit no.of api request to 100 per 5 minutes
const limiter = rateLimit({
  windowMs: 5 * 60 * 1000, // 5 minutes
  max: 100, // limit each IP to 100 requests per windowMs
});

app.use(limiter);

//Mount routes
app.use("/api/v1/reviews", reviews);
app.use("/api/v1/movies", movies);
app.use("/api/v1/trailers", trailer);
app.use("/api/v1/auth", auth);
app.use("/api/v1/location", location);
app.use("/api/v1/rating", rating);
app.use("/api/v1/watchlist", watchlist);
app.use("/api/v1/user", user);
//errorHandler
app.use(errorHandler);

const PORT = process.env.PORT || 5000;

const server = app.listen(
  PORT,
  console.log(
    `server is running in ${process.env.NODE_ENV} mode on port ${PORT}`
  )
);

process.on("unhandledRejection", (err, promise) => {
  console.log(`Error : ${err.message}`);
  server.close(() => process.exit(1));
});
