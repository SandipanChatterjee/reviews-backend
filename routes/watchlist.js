const express = require("express");
const router = express.Router({ mergeParams: true });
const { protect } = require("../middleware/auth");

const {
  createWatchList,
  updateWatchList,
  getWatchList,
} = require("../controller/watchlist");

router.route("/").get(protect, getWatchList).post(protect, createWatchList);
router.route("/:id").put(protect, updateWatchList);

module.exports = router;
