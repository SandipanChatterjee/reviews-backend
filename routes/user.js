const express = require("express");
const router = express.Router({ mergeParams: true });

const rating = require("./rating");
const watchlist = require("./watchlist");

// Re-route into other resource routers
router.use("/:userId/rating", rating);
router.use("/:userId/watchlist", watchlist);

module.exports = router;
